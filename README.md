# sv client

This repo contains the Dart server and browser client for Sourcevoid's API. It is used by: 

- [sv_cli](https://github.com/Sourcevoid/sv_cli) (Sourcevoid CLI App)
- [cloud.sourcevoid.com](https://cloud.sourcevoid.com/) (Sourcevoid Web App) 

## Example usage

For example usage, look at the [Sourcevoid CLI app](https://github.com/Sourcevoid/sv_cli).


